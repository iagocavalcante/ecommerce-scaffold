<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 28 Apr 2017 02:02:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CupomDesconto
 * 
 * @property int $id
 * @property string $nome
 * @property string $localizador
 * @property float $desconto
 * @property string $modo_desconto
 * @property float $limite
 * @property string $modo_limite
 * @property \Carbon\Carbon $dthr_validade
 * @property string $ativo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $pedido_produtos
 *
 * @package App\Models
 */
class CupomDesconto extends Eloquent
{
	protected $casts = [
		'desconto' => 'float',
		'limite' => 'float'
	];

	protected $dates = [
		'dthr_validade'
	];

	protected $fillable = [
		'nome',
		'localizador',
		'desconto',
		'modo_desconto',
		'limite',
		'modo_limite',
		'dthr_validade',
		'ativo'
	];

	public function pedido_produtos()
	{
		return $this->hasMany(\App\Models\PedidoProduto::class);
	}
}
