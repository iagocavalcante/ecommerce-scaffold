<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Request as Req;
use Illuminate\Support\Facades\Auth;
use App\Models\Pedido;
use App\Models\Pagamento;

class PagamentoController extends Controller {

	function __construct() {
        // obriga estar logado;
		$this->middleware('auth');
	}

	public function checkout() {

		$pedidos = Pedido::where([
			'status'  => 'RE',
			'user_id' => Auth::id()
			])->get();
		$ip = PagamentoController::getIp();

		return view('carrinho.pagamento', compact('pedidos', 'ip'));
	}

	public static function getIp(){
		foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
			if (array_key_exists($key, $_SERVER) === true){
				foreach (explode(',', $_SERVER[$key]) as $ip){
                $ip = trim($ip); // just to be safe
                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                	return $ip;
                }
            }
        }
    }
}

}
